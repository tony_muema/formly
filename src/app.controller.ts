
import { Body, Controller, Get, Post } from '@nestjs/common';
import { AppService } from './app.service';
import { FormlyFieldConfig } from '@ngx-formly/core';


@Controller()

export class AppController {
  constructor(private readonly appService: AppService) { }

  //model
  FormEntries: FormlyBackend[] = [

  ]; 

  @Get()
  getHello(): string {
    return this.appService.getHello();
  }

  //controller
  @Post('form')
  createFormlyEntry(@Body() formlyEntry) {
    this.FormEntries.push(formlyEntry);
  }

  @Get('forms')
  getAllFormlyEntries() {
    return this.FormEntries;
  }

  @Get()
  getByTitle() {
    /* 
    return this.FormEntries({title: title})
    */
  }

}


//dto
interface FormlyBackend {
  title: string,
  fields: FormlyFieldConfig[]
}
