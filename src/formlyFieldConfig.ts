

export interface FormlyFieldConfig {
    /**
     * The model that stores all the data, where the model[key] is the value of the field
     */
    readonly model?: any;
    /**
     * The parent field.
     */
    readonly parent?: FormlyFieldConfig;
    readonly options?: FormlyFormOptions;
   // readonly form?: FormGroup;
    /**
     * The key that relates to the model. This will link the field value to the model
     */
    key?: string | number | string[];
    /**
     * This allows you to specify the `id` of your field. Note, the `id` is generated if not set.
     */
    id?: string;
    /**
     * If you wish, you can specify a specific `name` for your field. This is useful if you're posting the form to a server using techniques of yester-year.
     */
    name?: string;
    /**
     * This is reserved for the templates. Any template-specific options go in here. Look at your specific template implementation to know the options required for this.
     */
    templateOptions?: FormlyTemplateOptions;
    optionsTypes?: string[];
    /**
     * An object with a few useful properties
     * - `validation.messages`: A map of message names that will be displayed when the field has errors.
     * - `validation.show`: A boolean you as the developer can set to force displaying errors whatever the state of field. This is useful when you're trying to call the user's attention to some fields for some reason.
     */
    validation?: {
        messages?: {
            [messageProperties: string]: any['message'];
        };
        required?: boolean;
        [additionalProperties: string]: any;
    };
    /**
     * Used to set validation rules for a particular field.
     * Should be an object of key - value pairs. The value can either be an expression to evaluate or a function to run.
     * Each should return a boolean value, returning true when the field is valid. See Validation for more information.
     *
     * {
     *   validation?: (string | ValidatorFn)[];
     *   [key: string]: ((control: AbstractControl, field: FormlyFieldConfig) => boolean) | ({ expression: (control: AbstractControl, field: FormlyFieldConfig) => boolean, message: ValidationMessageOption['message'] });
     * }
     */
    validators?: any;
    /**
     * Use this one for anything that needs to validate asynchronously.
     * Pretty much exactly the same as the validators api, except it must be a function that returns a promise.
     *
     * {
     *   validation?: (string | AsyncValidatorFn)[];
     *   [key: string]: ((control: AbstractControl, field: FormlyFieldConfig) => Promise<boolean> | Observable<boolean>) | ({ expression: (control: AbstractControl, field: FormlyFieldConfig) => Promise<boolean>, message: string });
     * }
     */
    asyncValidators?: any;
    /**
     * Can be set instead of `type` to render custom html content.
     */
    template?: string;
    /**
     *  It is expected to be the name of the wrappers.
     *  The formly field template will be wrapped by the first wrapper, then the second, then the third, etc.
     *  You can also specify these as part of a type (which is the recommended approach).
     */
    wrappers?: string[];
    /**
     * Whether to hide the field. Defaults to false. If you wish this to be conditional use `hideExpression`
     */
    hide?: boolean;
    /**
     * Conditionally hiding Field based on values from other Fields
     */
    hideExpression?: boolean | string | ((model: any, formState: any, field?: FormlyFieldConfig) => boolean);
    /**
     * An object where the key is a property to be set on the main field config and the value is an expression used to assign that property.
     */
    expressionProperties?: {
        [property: string]: string | ((model: any, formState: any, field?: FormlyFieldConfig) => any) ;
    };
    /**
     * This is the [FormControl](https://angular.io/api/forms/FormControl) for the field.
     * It provides you more control like running validators, calculating status, and resetting state.
     */
    formControl?: any;
    /**
     * You can specify your own class that will be applied to the `formly-field` component.
     */
    className?: string;
    /**
     * Specify your own class that will be applied to the `formly-group` component.
     */
    fieldGroupClassName?: string;
    fieldGroup?: FormlyFieldConfig[];
    fieldArray?: FormlyFieldConfig;
    type?: string;
    focus?: boolean;
    modelOptions?: {
        debounce?: {
            default: number;
        };
        updateOn?: 'change' | 'blur' | 'submit';
    };
    hooks?: FormlyLifeCycleOptions<FormlyHookFn>;    
    lifecycle?: FormlyLifeCycleOptions;
    defaultValue?: any;    
    parsers?: ((value: any) => {})[];
}
export interface ExpressionPropertyCache {
    expression: (model: any, formState: any, field: FormlyFieldConfigCache) => boolean;
    expressionValue?: any;
}
export interface FormlyFieldConfigCache extends FormlyFieldConfig {
    parent?: FormlyFieldConfigCache;
    options?: FormlyFormOptionsCache;
    _expressionProperties?: {
        [property: string]: ExpressionPropertyCache;
    };
    _hide?: boolean;
    _validators?: any[];
    _asyncValidators?: any[];
    //_componentRefs?: ComponentRef<FieldType>[];
    _keyPath?: {
        key: FormlyFieldConfig['key'];
        path: string[];
    };
}
export declare type FormlyAttributeEvent = (field: FormlyFieldConfig, event?: any) => void;
export interface FormlyTemplateOptions {
    type?: string;
    label?: string;
    placeholder?: string;
    disabled?: boolean;
    options?: any[] ;
    rows?: number;
    cols?: number;
    description?: string;
    hidden?: boolean;
    max?: number;
    min?: number;
    minLength?: number;
    maxLength?: number;
    pattern?: string | RegExp;
    required?: boolean;
    tabindex?: number;
    readonly?: boolean;
    attributes?: {
        [key: string]: string | number;
    };
    step?: number;
    focus?: FormlyAttributeEvent;
    blur?: FormlyAttributeEvent;
    keyup?: FormlyAttributeEvent;
    keydown?: FormlyAttributeEvent;
    click?: FormlyAttributeEvent;
    change?: FormlyAttributeEvent;
    keypress?: FormlyAttributeEvent;
    templateManipulators?: any;
    [additionalProperties: string]: any;
}
export interface FormlyLifeCycleFn {
    (form?: any, field?: FormlyFieldConfig, model?: any, options?: FormlyFormOptions): void;
}
export interface FormlyHookFn {
    (field?: FormlyFieldConfig): void;
}
export interface FormlyLifeCycleOptions<T = FormlyLifeCycleFn> {
    onInit?: T;
    onChanges?: T;
    afterContentInit?: T;
    afterViewInit?: T;
    onDestroy?: T;
    [additionalProperties: string]: any;
    /** @deprecated */
    doCheck?: T;
    /** @deprecated */
    afterContentChecked?: T;
    /** @deprecated */
    afterViewChecked?: T;
}
export interface FormlyFormOptionsCache extends FormlyFormOptions {
    _checkField?: (field: FormlyFieldConfigCache, ignoreCache?: boolean) => void;
    _markForCheck?: (field: FormlyFieldConfigCache) => void;
    _buildForm?: () => void;
    _buildField?: (field: FormlyFieldConfigCache) => FormlyFieldConfigCache;
    _resolver?: any;
    _injector?: any;
    _hiddenFieldsForCheck?: FormlyFieldConfigCache[];
    _initialModel?: any;
}
export interface FormlyFormOptions {
    updateInitialValue?: () => void;
    resetModel?: (model?: any) => void;
    formState?: any;
    fieldChanges?: any;
    fieldTransform?: (fields: FormlyFieldConfig[], model: any, form: any | any, options: FormlyFormOptions) => FormlyFieldConfig[];
    //showError?: (field: FieldType) => boolean;
    parentForm?: any | null;
}
export interface FormlyValueChangeEvent {
    field: FormlyFieldConfig;
    type: string;
    value: any;
    [meta: string]: any;
}
